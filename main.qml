import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: window
    visible: true
    visibility: "FullScreen"
    width: 480
    height: 320
    title: qsTr("Tabs")



    Connections {
        target: dataProvider

        onWaterTempChanged:{
            waterProgress.value = val
            waterValueText.text = val + "°C"
            if(val < 0.5 * waterProgress.maximumValue){
                waterContainer.waterColor = "green"
            }
            else if(val < 0.8  * waterProgress.maximumValue){
                waterContainer.waterColor = "yellow"

            }
            else{
                waterContainer.waterColor = "red"

            }
        }
        onOilTempChanged: {
            oilProgress.value = val
            oilValueText.text = val + "°C"
            if(val < 0.5 * oilProgress.maximumValue){
                oilContainer.oilColor = "green"
            }
            else if(val < 0.8  * oilProgress.maximumValue){
                oilContainer.oilColor = "yellow"

            }
            else{
                oilContainer.oilColor = "red"

            }
        }

        onRpmChanged: {
            console.log("rpm changed " + val);
            speedometer.value = val/1000;
        }
        onGearChanged: {
            console.log("gear changed " + val);
            gearText.text = val;
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
//        currentIndex: 1

        Page1Form {

            Rectangle {
                anchors.fill: parent
                color:"black"
            }

            Rectangle {
                id: waterContainer

                height: 200
                width: 24

                anchors.top: parent.top
                anchors.topMargin: 50
                anchors.left: parent.left
                anchors.leftMargin: 20

                property color waterColor: "green"

                ProgressBar {
                    id: waterProgress
                    minimumValue: 0
                    maximumValue: 100
                    value: 25

                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent
                    orientation: Qt.Vertical

                    style: ProgressBarStyle {
                        progress: Rectangle {
                            id: waterProgressColor
                            color: waterContainer.waterColor
                            border.color: "white"
                        }
                        background: Rectangle {
                                radius: 2
                                color: "black"
                                border.color: "white"
                                border.width: 1
                            }


                        }
                }

                Image {
                    id: waterIcon
                    source: "images/water_temp.png"
                    width: 40
                    height: 40
                    anchors.left: waterProgress.right
                    anchors.leftMargin: 5
                    anchors.verticalCenter: parent.verticalCenter
//                    height: 20
                    Text {
                       id: waterValueText
                       font.pixelSize: 20
                       text: "50°C"
                       color: "white"
                       horizontalAlignment: Text.AlignHCenter
                       anchors.horizontalCenter: parent.horizontalCenter
                       anchors.top: parent.bottom
                       anchors.topMargin: 5
                    }

                }
            } Rectangle {
                id: oilContainer

                height: 200
                width: 24

                anchors.top: parent.top
                anchors.topMargin: 50
                anchors.right: parent.right
                anchors.rightMargin: 20

                property color oilColor: "green"

                ProgressBar {
                    id: oilProgress
                    minimumValue: 0
                    maximumValue: 150
                    value: 60

                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent

                    orientation: Qt.Vertical
                    style: ProgressBarStyle {
                        progress: Rectangle {
                            color: oilContainer.oilColor
                            border.color: "white"
                        }
                        background: Rectangle {
                                radius: 2
                                color: "black"
                                border.color: "white"
                                border.width: 1
                            }


                        }
                }


                Image {
                    id: oilIcon
                    source: "images/oil_temp.png"
                    anchors.right: oilProgress.left
                    anchors.rightMargin: 5
                    anchors.verticalCenter: parent.verticalCenter
                    width: 50
                    height: 50

                    Text {
                       id: oilValueText
                       font.pixelSize: 20
                       text: "60°C"
                       color: "white"
                       horizontalAlignment: Text.AlignRight
                       anchors.horizontalCenter: parent.horizontalCenter
                       anchors.top: parent.bottom
                       anchors.topMargin: 5
                    }

                }
            }



            CircularGauge {
                id: speedometer
                style: CircularGaugeStyle {
                        tickmarkStepSize: 1
                        labelStepSize: 1
                         needle: Rectangle {
                             y: outerRadius * 0.15
                             implicitWidth: outerRadius * 0.05
                             implicitHeight: outerRadius * 0.98
                             antialiasing: true
                             color: Qt.rgba(1, 0, 0, 1)
                         }


                }
                minimumValue: 0
                maximumValue: 15
                value: 3.3
                tickmarksVisible: true
                height: parent.height * 0.75
                anchors.top: parent.top
                anchors.topMargin: height*0.05
                anchors.centerIn: parent

                Text {
                   id: gearText
                   font.pixelSize: 50
                   text: "5"
                   color: "white"
                   horizontalAlignment: Text.AlignRight
                   anchors.horizontalCenter: parent.horizontalCenter
                   anchors.top: speedometer.top
                   anchors.topMargin: 50
               }
                Text {
                   font.pixelSize: 15
                   text: "RPM x 1000"
                   color: "white"
                   horizontalAlignment: Text.AlignRight
                   anchors.horizontalCenter: parent.horizontalCenter
                   anchors.top: speedText.top
                   anchors.topMargin: 100
               }Text {
                   font.pixelSize: 15
                   text: "RPM"
                   color: "white"
                   horizontalAlignment: Text.AlignRight
                   anchors.horizontalCenter: parent.horizontalCenter
                   anchors.top: speedText.top
                   anchors.topMargin: 50
               }
               Text {
                   id: speedText
                   font.pixelSize: 50
                   text: Math.round(speedometer.value * 1000)
                   color: "white"
                   horizontalAlignment: Text.AlignRight
                   anchors.horizontalCenter: parent.horizontalCenter
                   anchors.top: parent.verticalCenter
                   anchors.topMargin: 10
               }
            }
            MouseArea {
                anchors.fill: parent
//                cursorShape: Qt.BlankCursor
                onClicked: {
                    console.log(speedometer.value)
                    dataProvider.moveCursor()
                }
            }

        }

        Page2Form {
            id: page2
            property bool isFullscreen: window.visibility === "FullScreen"

            btnPlus.onClicked: {

                var interval = parseInt(page2.tfInterval.text);
                interval += 100;
                page2.tfInterval.text = interval;
               // dataProvider.setInterval(interval);

            }
            btnMinus.onClicked: {

                var interval = parseInt(page2.tfInterval.text);
                interval -= 100;
                page2.tfInterval.text = interval;


            }

            tfInterval.onTextChanged: {
                var interval = parseInt(page2.tfInterval.text);
                if(interval <= 0){
                    interval = 1;
                     page2.tfInterval.text = interval;
                }
                dataProvider.setInterval(interval);
            }

            Button {
                text: "toggle fullscreen"
                      onClicked: {
                          if(page2.isFullscreen){
                              window.visibility = "Windowed"
                              page2.isFullscreen = false
                          }
                          else{
                              window.visibility = "FullScreen"
                              page2.isFullscreen = true
                          }
                      }

                  }
        }
        Page3Form{


        }
    }

//    Button {
//           text: "toggle fullscreen"
//           onClicked: {
//               console.log("clicked");
//           }

//       }

//    TabBar {
//        id: tabBar
//        currentIndex: swipeView.currentIndex
//        anchors.bottom: parent.bottom

//        TabButton {
//            text: qsTr("Mains")
//        }
//        TabButton {
//            text: qsTr("Sensors")
//        }
//        TabButton {
//            text: qsTr("Camera")
//        }
//    }
}
