import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: page
    width: 480
    height: 500
    clip: false
    visible: true
    enabled: true
    font.strikeout: false

    Label {
        text: qsTr("42")
        anchors.verticalCenterOffset: 0
        anchors.horizontalCenterOffset: 1
        font.pointSize: 50
        anchors.centerIn: parent

        Text {
            id: text1
            x: -125
            y: -35
            width: 104
            height: 44
            text: qsTr("Speed:")
            anchors.verticalCenterOffset: 0
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 33
        }
    }
}
