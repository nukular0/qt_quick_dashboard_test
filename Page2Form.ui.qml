import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: pg2
    width: 600
    height: 400
    property alias btnPlus: btnPlus
    property alias btnMinus: btnMinus
    property alias tfInterval: tfInterval

    header: Label {
        text: qsTr("Camera")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    TextField {
        id: tfInterval
        x: 200
        y: 129
        text: qsTr("500")
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: btnPlus
        x: 252
        y: 72
        width: 97
        height: 32
        text: qsTr("+100ms")
    }

    Button {
        id: btnMinus
        x: 252
        y: 198
        width: 97
        height: 32
        text: qsTr("-100ms")
    }
}
