#include "dataprovider.h"
#include <QDebug>
#include <QRandomGenerator>
#include <xenstore.h>
#include <QThread>


void Dataprovider::xenstoreThreadFunc(){
    xs_transaction_t xst;
    struct xs_handle *xs;
    int err;
    int fd;
    int ret;
    fd_set set;
    char **vec;
    unsigned int num_strings;
    struct timeval tv = {.tv_sec = 5, .tv_usec = 0 };

    xs = xs_open(0);
    if(!xs){
        qDebug() << "Could not open Xenstore handle";
        return;
    }

    /* Create a watch on /sensors */
    err = xs_watch(xs, "/sensors", "");

    if(!err){
        qDebug() << "Error creating XS Watch";
    }
    else{
        qDebug() << "XS watch registered";
    }

    fd = xs_fileno(xs);

    while(!future.isCanceled()){
        FD_ZERO(&set);
        FD_SET(fd, &set);

         ret = select(fd + 1, &set, NULL, NULL, NULL);
         qDebug() << "ret: " << ret;
          vec = xs_read_watch(xs, &num_strings);
          readSensorDataFromXS();

    }
    xs_close(xs);
}

Dataprovider::Dataprovider(QObject *parent) : QObject(parent)
{
    connect(&refreshDataTimer, SIGNAL(timeout()), this, SLOT(refreshDataTimerTimeout()));

    xs = xs_open(0);
    if(!xs){
        qDebug() << "Could not open Xenstore handle";
        return;
    }

    refreshDataTimer.start(1000);
//    setInterval(1000);

    future = QtConcurrent::run(this, &Dataprovider::xenstoreThreadFunc);
    qDebug() << "hello from GUI thread " << QThread::currentThread();

}

Dataprovider::~Dataprovider()
{
    future.cancel();
    // Write something to /sensor path so that select() in xenstoreThreadFunc() will unblock
    xs_write(xs, XBT_NULL, "/sensors/null", "0", 1);
    future.waitForFinished();
    xs_close(xs);

}

void Dataprovider::refreshDataTimerTimeout(){
    QCursor cursor;
    cursor.setPos(10, 10);
}



void Dataprovider::readSensorDataFromXS(){
    xs_transaction_t xst;
    char *rpm_buf, *tps_buf, *watert_buf, *oilt_buf, *airt_buf, *gear_buf;
    unsigned int len;


    qDebug() << "readSensorDataFromXS";

    xst = xs_transaction_start(xs);
    rpm_buf = (char*)xs_read(xs, xst, "/sensors/ecu/rpm", &len);
    tps_buf = (char*)xs_read(xs, xst, "/sensors/ecu/tps_percent", &len);
    watert_buf = (char*)xs_read(xs, xst, "/sensors/ecu/water_temp_c", &len);
    oilt_buf = (char*)xs_read(xs, xst, "/sensors/ecu/oil_temp_c", &len);
    airt_buf = (char*)xs_read(xs, xst, "/sensors/ecu/air_temp_c", &len);
    gear_buf = (char*)xs_read(xs, xst, "/sensors/ecu/gear", &len);
    xs_transaction_end(xs, xst, false);

   // xs_close(xs);

    if(rpm_buf){
        rpm = QString(rpm_buf).toInt();
        qDebug() << "rpm" <<rpm;
    }
    if(tps_buf){
        tps_percent = QString(tps_buf).toInt();
        qDebug() << "tps_percent" <<tps_percent;
    }
    if(watert_buf){
        water_t = QString(watert_buf).toInt();
        qDebug() << "water_t" <<water_t;
    }
    if(oilt_buf){
        oil_t = QString(oilt_buf).toInt();
        qDebug() << "oil_t" <<oil_t;
    }
    if(airt_buf){
        air_t = QString(airt_buf).toInt();
        qDebug() << "air_t" <<air_t;
    }
    if(gear_buf){
        gear = QString(gear_buf).toInt();
        qDebug() << "gear" << gear;
    }


    free(rpm_buf);
    free(tps_buf);
    free(watert_buf);
    free(oilt_buf);
    free(airt_buf);
    free(gear_buf);
    emit(waterTempChanged(water_t));
    emit(oilTempChanged(oil_t));
    emit(airTempChanged(air_t));
    emit(rpmChanged(rpm));
    emit(tpsChanged(tps_percent));
    emit(gearChanged(gear));
}

void Dataprovider::setInterval(int msec){
    qDebug() << "Interval set to " << msec;
    refreshDataTimer.setInterval(msec);
}

