#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H

#include <QObject>
#include <QTimer>

#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QCursor>

class Dataprovider : public QObject
{
    Q_OBJECT

private:

    int water_t;
    int oil_t;
    int air_t;
    int rpm;
    int tps_percent;
    int gear;

    struct xs_handle *xs = NULL;

    QTimer refreshDataTimer;
    QFuture<void> future;

    void readSensorDataFromXS();
    QCursor cursor;

public:
    explicit Dataprovider(QObject *parent = nullptr);
    ~Dataprovider();
    Q_INVOKABLE void setInterval(int msec);

signals:
    void waterTempChanged(int val);
    void oilTempChanged(int val);
    void airTempChanged(int val);
    void rpmChanged(int val);
    void tpsChanged(int val);
    void gearChanged(int val);


public slots:
    void refreshDataTimerTimeout();
    void xenstoreThreadFunc();
    void moveCursor() { cursor.setPos(10, 10); }

};

#endif // DATAPROVIDER_H
