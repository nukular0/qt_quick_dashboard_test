#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <dataprovider.h>
#include <QQuickWindow>
#include <QCursor>

int main(int argc, char *argv[])
{
//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickWindow::setSceneGraphBackend(QSGRendererInterface::Software);
    QGuiApplication app(argc, argv);
//    app.setOverrideCursor( QCursor( Qt::BlankCursor ) );


    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    Dataprovider dp;
    context->setContextProperty("dataProvider", &dp);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
